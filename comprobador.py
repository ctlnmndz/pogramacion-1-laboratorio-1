#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Función para sacar promedio
def promedio(lista=[]): 
	#Sacar la cantidad de numeros que hay en la lista
	cantidad=len(lista)
	#Calcular la suma de la lista
	suma=sum(lista)
	#Calcular el promedio
	promedio=suma/cantidad
	return promedio

#Ingresamos la lista a calcular
lista=[2,6,8,2.5,9,4.8]
#Llamar la función
l= promedio(lista)
#Imprimir el promedio
print(l)

#Función para sacar el cuadrado de la lista
def cuadrado(lista=[]):
	#Sacar el largo de la lista
	cantidad=len(lista)
	i=0
	#ciclo para elevar al cuadrado
	for i in range (cantidad):
		numero=0
		numero=lista[i]
		numero=numero**2
		lista[i]=numero
	return lista

#Lista a calcular
lista=[2,3,4,5,6,7,8,9]
#Se llama la función 
a=cuadrado(lista)
#Se imprime la lista 
print(a)

#Funcion para calcular la palabra mayor
def cuenta_letra(lista=[]):
	#Se calcula la palabra mayor
	palabra_mayor=max(lista)
	return palabra_mayor

#Lista a calcular	
lista=['Hola','Catalina','Programar','Adios']
#Se llama la función
l=cuenta_letra(lista)
#Se imprime la palabra mayor
print(l)
		
		
	
